<body>
	<!-- LAYOUT / TYPO / CONTENT -->
	<div id="page-wrapper" class="page">
		<div id="header-wrapper">
			<header id="header" class="clearfix">
					<h1><a href="index.html" title="HTML5 Template">LogoImage</a></h1>
            </header>
		</div>
		<div id="main-wrapper">
			<div id="pages">
            <ul>
            	<li class="pages"><a href="">Page1</a></li>
                <li class="pages"><a href="">Page2</a></li>
                <li class="pages"><a href="">Page3</a></li>
                <li class="pages"><a href="">Page4</a></li>
                <li class="pages"><a href="">Page5</a></li>
            </ul>
		</div>
        
        <div id="content">
        <div id="Text">
        
        <h2> Main Title </h2>
        <hr/>

<p>Sprat longnose whiptail catfish, emperor bream, channel bass swamp-eel chum salmon surgeonfish alligatorfish, sheepshead earthworm eel fusilier fish snubnose eel white marlin white croaker? Tiger shovelnose catfish driftfish climbing perch wobbegong, California halibut weasel shark, riffle dace silver driftfish, sleeper shark, South American darter. Marine hatchetfish batfish barbeled dragonfish yellowhead jawfish. Steve fish handfish northern pike round stingray surfperch speckled trout, surf sardine sandfish? Zander sand diver sargassum fish seamoth yellowfin pike baikal oilfish nurseryfish upside-down catfish, ziege leaffish, "clownfish cichlid humuhumunukunukuapua'a fusilier fish." Blue whiting beluga sturgeon Black pickerel tilefish tetra; parasitic catfish worm eel, wallago nurse shark spiny eel Ganges shark?</p>

<p>Black triggerfish mud catfish cobbler trumpeter hairtail titan triggerfish torrent fish Black scabbardfish, ponyfish, razorfish bat ray. Ribbonfish cownose ray zebra lionfish requiem shark Ragfish Cherubfish squawfish Australian prowfish mahseer whale catfish boafish Pacific trout! Stickleback zebra loach cavefish snook velvet-belly shark. Sailbearer porgy dragonet snubnose parasitic eel capelin, kissing gourami, sand knifefish mudsucker galjoen fish pompano dolphinfish?</p>

<p>Pollock weever, emperor angelfish, rockweed gunnel pirate perch loosejaw; rivuline eulachon Bombay duck. Stonefish bluntnose knifefish; chain pickerel giant danio barfish, emperor bream; treefish tompot blenny electric eel. Barred danio sábalo common carp; sillago ghost knifefish, "lamprey Pacific herring louvar sweeper, finback cat shark." Aholehole codling trout cod pompano dolphinfish boxfish collared carpetshark forehead brooder scissor-tail rasbora.</p>
        </div>
        	<div id="media">
            <ul>
            
            	<li><img src="sites/all/themes/SiteJensS/content/images/donkey.png" title="DonkeyIsFunny" class="contentImg"/></li>
                <li><img src="sites/all/themes/SiteJensS/content/images/donkey.png" title="DonkeyIsFunny" class="contentImg2"/></li>
             </ul> 
            </div>
        </div>
        
        <div id="footer">
        <hr/>
        <p>Created by Jens Siebens for Artevelde Hogeschool. All rights reserved.</p>
        	<div id="logos">
            <ul>
            	<li class="links"><a href="http://www.arteveldehs.be/emc.asp?pageId=1942"><img src="sites/all/themes/SiteJensS/content/images/LOGO proDUCE BW.png" class="speciaal"/></a></li>
                <li class="links"><a href="http://www.arteveldehs.be/emc.asp?pageId=1834"><img src="sites/all/themes/SiteJensS/content/images/ARTEV_be_zwart_zonder_baseline.png" class="speciaal"/></a></li>
                <li class="links"><a href="https://www.facebook.com/"><img src="sites/all/themes/SiteJensS/content/images/facebook-logo-square-webtreatsetc.png"/></a></li>
                <li class="links"><a href="https://twitter.com/"><img src="sites/all/themes/SiteJensS/content/images/twitter-logo-square-black-white.png"/></a></li> 
                <li class="links"><a href="http://drupal.be/"><img src="sites/all/themes/SiteJensS/content/images/s_druplicon_0.png"/></a></li>
              </ul>  
        	</div>
        </div>
        </div>
	</div>
	<a href="#" class="scrolltotop"></a>
</body>